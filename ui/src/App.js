import React, { Component } from 'react';
import {
  BrowserRouter,
  Route,
  Redirect,
  Switch,
} from 'react-router-dom';
import { ToastContainer } from 'react-toastify';
import * as pages from './pages';
import './assets/css/main.css';
import 'basscss/css/basscss.min.css';
import 'react-toastify/dist/ReactToastify.css';

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <React.Fragment>
          <ToastContainer />
          <Switch>
            <Route path="/login" component={pages.Login} />
            <Redirect to="/login" />
          </Switch>
        </React.Fragment>
      </BrowserRouter>
    );
  }
}

export default App;
