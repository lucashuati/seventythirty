import React from 'react';
import { BasicText, asField } from 'informed';


export default asField(({
  fieldState, field, label, helpBlock, numbers, ...props
  }) => (
    <div>
      <label htmlFor={field}>{label}</label>
      <BasicText
        className={`input-text form-control ${fieldState.error ? 'has-error' : null}`}
        id={field}
        fieldState={fieldState}
        maxLength={numbers}
        {...props}
      />
      {helpBlock && <small className="form-text text-muted">{helpBlock}</small>}
      {fieldState.error && <small className={fieldState.error ? 'text-danger' : null}>{fieldState.error}</small>}
    </div>
));
