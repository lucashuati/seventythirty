import React, { Component, Fragment } from 'react';
import { Form } from 'informed';
import axios from 'axios';
import { get } from 'lodash';
import { TextInput } from '../components';
import { API_URL } from '../consts';
import { setFormErrors } from '../services/utils';

class Login extends Component {
  constructor(props) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
    this.setFormApi = this.setFormApi.bind(this);
  }

  handleClick(values) {
    axios.post(
      `${API_URL}/login/`,
      {...values}
    ).then(response => {
        console.log(response.data)
    }).catch(error => {
      const status = get(error, 'response.status');
      if (status === 400) {
        const errors = get(error, 'response.data', {});
        const { non_field_errors, ...fieldErrors } = errors;
        this.setState({ non_field_errors: non_field_errors || [] });
        setFormErrors(this.formApi, fieldErrors);
      }
    });
  }

  setFormApi(formApi) {
    this.formApi = formApi;
  }

  render() {
      return (
        <Fragment>
          <h2>Login</h2>
          <div className="login-container">
            <div className="card shadow login-box">
              <Form getApi={this.setFormApi} onSubmit={this.handleClick}>
                <div className="center">
                  <TextInput
                    field="username"
                    id="username"
                    placeholder="username"
                  />
                </div>
                <div className="center">
                  <TextInput
                    field="password"
                    id="password"
                    placeholder="password"
                    type="password"
                  />
                </div>
                <button className="btn btn-primary pull-right" type="submit">Submit</button>
              </Form>
            </div>
          </div>
        </Fragment>
      );
  }
}


export default Login;
