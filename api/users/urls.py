from rest_framework_jwt.views import obtain_jwt_token
from django.urls import path
from rest_framework import routers
from . import views

router = routers.SimpleRouter()
router.register(r'users', views.UserViewSet)

urlpatterns = [
    path('login/', obtain_jwt_token),
] + router.urls
