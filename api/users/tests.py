from rest_framework.test import APITestCase
from rest_framework import status


class UserTests(APITestCase):
    fixtures = ['testing.yaml']

    def test_login(self):
        token = self.client.post(
            '/login/',
            {
                'username': 'lucas',
                'password': 'r13r13r13'
            },
            format='json'
        ).data.get('token')
        self.client.credentials(
            HTTP_AUTHORIZATION=f'JWT {token}'
        )
        response = self.client.get('/users/details/')
        self.assertEqual(
            response.status_code,
            status.HTTP_200_OK
        )
