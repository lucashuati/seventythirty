from rest_framework import viewsets
from . import models, serializers
from rest_framework.decorators import action
from rest_framework.response import Response


class UserViewSet(viewsets.ModelViewSet):
    serializer_class = serializers.UserSerializer
    queryset = models.User.objects.all()

    @action(detail=False, methods=['get'])
    def details(self, request):
        return Response(
            serializers.UserSerializer(
                request.user
            ).data
        )
