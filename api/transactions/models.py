from django.db import models
from django.contrib.auth import get_user_model


class TransactionType(models.Model):
    description = models.CharField(max_length=100)
    percentage = models.IntegerField(null=True, blank=True)


class Transaction(models.Model):
    type = models.ForeignKey(TransactionType, on_delete=models.PROTECT)
    value = models.DecimalField(decimal_places=2, max_digits=10)
    description = models.TextField()
    user = models.ForeignKey(get_user_model(), on_delete=models.CASCADE)
    date = models.DateTimeField()
