from rest_framework import viewsets
from . import models, serializers


class TransactionsView(viewsets.ModelViewSet):
    def get_serializer_class(self):
        if self.action in ['retrive', 'list']:
            return serializers.TransactionSerializer
        return serializers.TransactionForm

    def get_queryset(self):
        return models.Transaction.objects.filter(
            user=self.user
        ).all()


class TransactionsTypeView(viewsets.ReadOnlyModelViewSet):
    serializer_class = serializers.TransactionTypeSerializer
    queryset = models.TransactionType.objects.all()
