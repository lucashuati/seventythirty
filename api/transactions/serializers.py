from rest_framework import serializers
from . import models


class TransactionSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Transaction
        fields = '__all__'
        depth = 1


class TransactionForm(serializers.ModelSerializer):
    class Meta:
        model = models.Transaction
        fields = (
            'type',
            'value',
            'description',
            'date',
        )

    def create(self, validated_data):
        validated_data.update({'user': self.context['request'].user})
        return models.Transaction.objects.create(**validated_data)


class TransactionTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.TransactionType
        fields = '__all__'
